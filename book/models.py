from django.db import models

# Create your models here.


class Book(models.Model):
    book_name = models.CharField(max_length=20)
    author_name = models.CharField(max_length=20)
    publisher_name = models.CharField(max_length=20)
    ISBN_no = models.CharField(max_length=13)
    Total_copies = models.IntegerField()
    available_copies = models.IntegerField()
    price_of_the_book = models.IntegerField()

    def __str__(self):
        return self.book_name





