# Generated by Django 3.0.3 on 2020-02-10 08:55

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('book_name', models.CharField(max_length=20)),
                ('author_name', models.CharField(max_length=20)),
                ('publisher_name', models.CharField(max_length=20)),
                ('isbn_no', models.CharField(max_length=13)),
                ('Total_copies', models.IntegerField()),
                ('available_copies', models.IntegerField()),
                ('price_of_the_book', models.IntegerField()),
            ],
        ),
    ]
