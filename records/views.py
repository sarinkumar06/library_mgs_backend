from datetime import date, timedelta

from django.shortcuts import render

# Create your views here.
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

import records
from book.models import Book
from records.models import Record
from records.serializer import RecordSerializer
from student.models import Student


class RecordViewSet(viewsets.ModelViewSet):
    queryset = Record.objects.all()
    serializer_class = RecordSerializer

    def create(self, request, *args, **kwargs):
        student = Student.objects.get(name=request.data["student"])
        book = Book.objects.get(book_name=request.data['book'])
        due_date = date.today() + timedelta(days=10)
        record = Record.objects.create(student=student, book = book, Due_date=due_date)
        book.available_copies -= 1
        book.save()
        return Response(RecordSerializer(record).data,status=status.HTTP_201_CREATED)

    @action(methods =['post'],detail=False)
    def fine(self, request):
        book = request.data['book']
        fn = Book.objects.get(book_name=book)
        student = Student.objects.get(name=request.data["student"])
        record = Record.objects.get(student=student, book=fn)
        record.fine()
        return Response(data=self.serializer_class(record).data)