
from datetime import datetime, timedelta

from django.db import models

# Create your models here.
from book.models import Book
from student.models import Student


class Record(models.Model):
    student = models.ForeignKey(Student, null=True, blank=True, on_delete=models.CASCADE)
    book = models.ForeignKey(Book, null=True, blank=True, on_delete=models.CASCADE)
    Issued_date = models.DateField(auto_now=True)
    Due_date = models.DateField()
    Returned_date = models.DateField(null=True, blank=True)
    Fine = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return str(self.student)


    def fine(self):
        self.Returned_date = datetime.today().date()
        a = self.Returned_date - self.Issued_date
        if a.days <=10:
            self.Fine=0
        else:
            self.Fine = a.days*2




















