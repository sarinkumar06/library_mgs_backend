from django.db import models

# Create your models here.


class Student(models.Model):
    name = models.CharField(max_length=20)
    reg_number = models.IntegerField()
    COMPUTER_SCIENCE = 'CS'
    CIVIL = 'CE'
    MECHANICAL = 'MEC'
    ELECTRICAL = 'EC'
    ELECTRICAL_ELECTRONICS = 'EEE'
    BIO_MEDICAL = 'BME'
    BRANCH_CHOICES = [
        (COMPUTER_SCIENCE, 'Computer_science'),
        (CIVIL, 'Civil'),
        (MECHANICAL, 'Mechanical'),
        (ELECTRICAL, 'Electrical'),
        (ELECTRICAL_ELECTRONICS, 'electrical_electronics'),
        (BIO_MEDICAL, 'Bio_medical'),
        ]
    branch = models.CharField(max_length=3, choices=BRANCH_CHOICES, default=None)

    def __str__(self):
        return self.name
